package se.experis;

import java.io.*;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

// Simple program that prompt the user to search through a JSON file.
public class Program {
    public static void main(String[] args) {

        System.out.println("Search for something... :");
        Scanner searchPhrase = new Scanner(System.in);
        String stringifySearchPhrase = searchPhrase.nextLine();

        searchFile(stringifySearchPhrase);

    }

    // Takes the phrase/word from user input and then loops through the array and search for the phrase in every object.
    private static void searchFile(String searchPhrase) {
        JSONParser parser = new JSONParser();
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("personData.json")){

            JSONArray jsonArray = (JSONArray) parser.parse(new InputStreamReader(inputStream, "UTF-8"));
            JSONParser jsonParser = new JSONParser();

            Object parseArrToObj = (Object) jsonParser.parse(String.valueOf(jsonArray));

            JSONArray jsonArr = (JSONArray) parseArrToObj;

            for (int i = 0; i < jsonArray.size(); i++) {

                JSONObject jsonObj = (JSONObject) jsonArr.get(i);

                if (jsonObj.containsValue(searchPhrase)) {
                    System.out.println(jsonObj);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
